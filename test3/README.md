

# 个人信息

班级：2020级软件工程2班

学号：202010414229

姓名：周钰全

# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

1. 使用使用sql-developer软件创建表；

   点击“SQL工作表”，选择连接；

   ![](pic01.png)

   输入参加表以及设置表分区的的SQL语句，再点击运行脚本即可创建表：

   ![](pic02.png)

   

   创建订单表(orders)的SQL语句如下：

   ```sql
   CREATE TABLE "SALE_ZYQ"."ORDERS"
   (
    order_id NUMBER(9, 0) NOT NULL
    , customer_name VARCHAR2(40 BYTE) NOT NULL 
    , customer_tel VARCHAR2(40 BYTE) NOT NULL 
    , order_date DATE NOT NULL 
    , employee_id NUMBER(6, 0) NOT NULL 
    , discount NUMBER(8, 2) DEFAULT 0 
    , trade_receivable NUMBER(8, 2) DEFAULT 0 
    , CONSTRAINT ORDERS_PK PRIMARY KEY 
     (
       ORDER_ID 
     )
   ) 
   TABLESPACE USERS 
   PCTFREE 10 INITRANS 1 
   STORAGE (   BUFFER_POOL DEFAULT ) 
   NOCOMPRESS NOPARALLEL 
   
   PARTITION BY RANGE (order_date) 
   (
    PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
    TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
    'NLS_CALENDAR=GREGORIAN')) 
    NOLOGGING
    TABLESPACE USERS
    PCTFREE 10 
    INITRANS 1 
    STORAGE 
   ( 
    INITIAL 8388608 
    NEXT 1048576 
    MINEXTENTS 1 
    MAXEXTENTS UNLIMITED 
    BUFFER_POOL DEFAULT 
   ) 
   NOCOMPRESS NO INMEMORY  
   , PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
   TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
   'NLS_CALENDAR=GREGORIAN')) 
   NOLOGGING
   TABLESPACE USERS
   , PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
   TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
   'NLS_CALENDAR=GREGORIAN')) 
   NOLOGGING 
   TABLESPACE USERS
   );
   
   ALTER TABLE sale_zyq.orders ADD PARTITION partition_before_2022
   VALUES LESS THAN(
   TO_DATE('2022-01-01 00:00:00','YYYY-MM-DD  HH24:MI:SS', 
   'NLS_CALENDAR=GREGORIAN'))
   NOLOGGING 
   TABLESPACE USERS;
   ```

   该SQL语句创建表之后，令表使用了 "USERS" 表空间，在这个表空间中的初始空闲空间为10％，每个表可以有一个并行操作，使用默认的缓冲池。

   令表使用了基于 "order_date" 列的分区，该列的日期范围被划分为三个分区：在2016年之前，2016年之前到2020年之前，以及在2021年之前。

   再使用ALTER TABLE 命令添加了一个新的分区，该分区存储在 "USERS" 表空间中，并定义为存储在2022年之前的行。

   

   创建订单详表(order_details)的SQL语句：

   ```sql
   CREATE TABLE "SALE_ZYQ"."ORDER_DETAILS"
   (
   	id NUMBER(9, 0) NOT NULL 
   	, order_id NUMBER(10, 0) NOT NULL
   	, product_id VARCHAR2(40 BYTE) NOT NULL 
   	, product_num NUMBER(8, 2) NOT NULL 
   	, product_price NUMBER(8, 2) NOT NULL 
   	, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
     (
       id 
     )
   	,CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
   	REFERENCES sale_zyq.orders  (  order_id   )
   	ENABLE
   ) 
   TABLESPACE USERS 
   PCTFREE 10 INITRANS 1 
   STORAGE ( BUFFER_POOL DEFAULT ) 
   NOCOMPRESS NOPARALLEL
   PARTITION BY REFERENCE (order_details_fk1);
   ```

   该SQL语句创建表之后，令表使用了 "USERS" 表空间，在这个表空间中的初始空闲空间为10％，每个表可以有一个并行操作，使用默认的缓冲池。

   表使用了基于外键 "order_details_fk1" 的引用分区，这个外键引用了另一个表 "ORDERS" 的 "order_id" 列。这意味着 "ORDER_DETAILS" 表的数据将根据 "ORDERS" 表的分区自动分区。因为表使用引用分区，所以它不需要分区键列。

   

2. 给表orders.customer_name增加B_Tree索引，因为B_Tree索引为默认索引并且顾客名可能相同，所以设置索引类型为不唯一。

   ![](pic03.png)

   导出的SQL语句：

   ```sql
   CREATE INDEX SALE_ZYQ.ORDERS_INDEX1 ON SALE_ZYQ.ORDERS (CUSTOMER_NAME ASC);
   ```

3. 设置orders.order_id和order_details.id自增。

   - 点击左上角的“新建”，再点击“数据库对象”，再选择“序列”来，创建序列，定义一个增长逻辑。

     ![](pic04.png)

     ![](pic05.png)

     导出的SQL语句：

     ```sql
     CREATE SEQUENCE SALE_ZYQ.SEQUENCE_ID INCREMENT BY 1 MAXVALUE 999999999 MINVALUE 1 CACHE 20 ORDER;
     ```

   - 创建触发器，将增长逻辑与列绑定，并说明何时触发增长逻辑。选中主键，选择“身份列”，设置类型为“列序列”，将序列设置为已创建的序列。

     ![](pic06.png)

     导出的SQL语句：

     ```sql
     CREATE TRIGGER SALE_ZYQ.ORDERS_TRG 
     BEFORE INSERT ON SALE_ZYQ.ORDERS 
     FOR EACH ROW 
     BEGIN
       <<COLUMN_SEQUENCES>>
       BEGIN
         IF INSERTING AND :NEW.ORDER_ID IS NULL THEN
           SELECT SEQUENCE_ID.NEXTVAL INTO :NEW.ORDER_ID FROM SYS.DUAL;
         END IF;
       END COLUMN_SEQUENCES;
     END;
     ```
     
     也可以选择表自动创建的序列。
     
     ![](pic07.png)
     
     导出的SQL语句：
     
     ```sql
     CREATE SEQUENCE SALE_ZYQ.ORDER_DETAILS_SEQ;
     
     CREATE TRIGGER SALE_ZYQ.ORDER_DETAILS_TRG 
     BEFORE INSERT ON SALE_ZYQ.ORDER_DETAILS 
     FOR EACH ROW 
     BEGIN
       <<COLUMN_SEQUENCES>>
       BEGIN
         IF INSERTING AND :NEW.ID IS NULL THEN
           SELECT ORDER_DETAILS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
         END IF;
       END COLUMN_SEQUENCES;
     END;
     ```

4. 创建并允许SQL脚本：随机生成数据，并且数据应该能并平均分布到各个分区。orders表插入40万行数据，order_details表插入200万行数据（每个订单对应5个order_details）。

   增加用户在表空间中的配额：

   ```sql
   ALTER USER sale_zyq  QUOTA 1G ON USERS;
   ```

   插入 40 万行数据到 orders 表：

   ```sql
   DECLARE
   v_order_id NUMBER;
   BEGIN
   FOR i IN 1..400000 LOOP
   v_order_id := i;
   INSERT INTO sale_zyq.orders
   (order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable)
   VALUES
   (v_order_id, 'Customer ' || TRIM(TO_CHAR(i, '000000')), 'Tel ' || TRIM(TO_CHAR(i, '000000')),
   TO_DATE('2015-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(1, 2555), -- 2015 年到 2022 年之间随机日期
   TRUNC(DBMS_RANDOM.VALUE(1, 1000000)), -- 随机员工 ID
   TRUNC(DBMS_RANDOM.VALUE(0, 100)), -- 随机折扣
   TRUNC(DBMS_RANDOM.VALUE(100, 10000))); -- 随机应收金额
   IF MOD(i, 10000) = 0 THEN
   COMMIT;
   END IF;
   END LOOP;
   COMMIT;
   END;
   ```

   插入 200 万行数据到 order_details 表：

   ```sql
   DECLARE
   v_id NUMBER;
   v_order_id NUMBER;
   BEGIN
   FOR i IN 1..400000 LOOP
   v_order_id := i;
   FOR j IN 1..5 LOOP
   v_id := (i - 1) * 5 + j;
   INSERT INTO sale_zyq.order_details
   (id, order_id, product_id, product_num, product_price)
   VALUES
   (v_id, v_order_id, 'Product ' || TRIM(TO_CHAR(j, '000')), TRUNC(DBMS_RANDOM.VALUE(1, 10)), TRUNC(DBMS_RANDOM.VALUE(10, 100)));
   END LOOP;
   IF MOD(i, 10000) = 0 THEN
   COMMIT;
   END IF;
   END LOOP;
   COMMIT;
   END;
   ```

   

5. 用户sale_zyq默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户sale_zyq授予以下视图的选择权限：

   ```sql
   $ sqlplus sys/123@localhost/pdborcl as sysdba
   @$ORACLE_HOME/sqlplus/admin/plustrce.sql
   create role plustrace;
   GRANT SELECT ON v_$sesstat TO plustrace;
   GRANT SELECT ON v_$statname TO plustrace;
   GRANT SELECT ON v_$mystat TO plustrace;
   GRANT plustrace TO dba WITH ADMIN OPTION;
   GRANT plustrace TO sale_zyq;
   GRANT SELECT ON v_$sql TO sale_zyq;
   GRANT SELECT ON v_$sql_plan TO sale_zyq;
   GRANT SELECT ON v_$sql_plan_statistics_all TO sale_zyq;
   GRANT SELECT ON v_$session TO sale_zyq;
   GRANT SELECT ON v_$parameter TO sale_zyq; 
   ```

6. 使用两个表的联合查询的语句进行查询：

   ```SQL
   SET AUTOTRACE ON;
   SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
   FROM sale_zyq.orders o
   JOIN sale_zyq.order_details od ON o.order_id = od.order_id
   WHERE o.trade_receivable=5000;
   ```

   查询的统计信息

   ![](pic09.png)

7. 给两个表创建一个副表，再次进行查询得到以下统计信息：

   ![](pic10.png)

## 实验结论

从统计信息来看，分区表的 consistent gets 数量为 12675，而没有分区的表的 consistent gets 数量为 12537。这表示分区表的查询需要更多的 I/O 操作，因为分区表被划分为多个部分，查询需要在多个部分之间进行，导致需要更多的 consistent gets 操作。而没有分区的表则只需要进行单一的 I/O 操作，因此需要的 consistent gets 数量更少。

此外，分区表的 CPU 使用量、DB time 和递归调用次数都比没有分区的表高，这意味着查询分区表需要更多的计算资源。而没有分区的表则需要更多的物理 I/O 操作，因为它的 cell physical IO interconnect bytes 和 physical read total bytes 都比分区表高。

因此，可以得出结论：使用分区表可以提高查询效率和性能，但同时也需要更多的计算资源。而没有分区的表虽然需要更多的物理 I/O 操作，但在某些情况下可能会更加适合使用。选择使用分区表还是非分区表取决于特定的应用场景和需求。
