-- 查询某个员工的部门名称
SELECT employee_id, name, study.sales_system_pkg.get_employee_department(employee_id) AS department_name
FROM study.employees;

-- 查询某个客户的订单总金额
SELECT customer_id, customer_name, study.sales_system_pkg.get_customer_order_total(customer_id) AS order_total
FROM study.customer;

-- 查询某个客户的订单数量
SELECT customer_id, customer_name, study.sales_system_pkg.get_customer_order_count(customer_id) AS order_count
FROM study.customer;

-- 查询某个员工的销售总额
SELECT employee_id, name, study.sales_system_pkg.get_employee_sales_total(employee_id) AS sales_total
FROM study.employees;

-- 查询某个产品的销售数量
SELECT product_id, product_name, study.sales_system_pkg.get_product_sales_count(product_id) AS sales_count
FROM study.products;

-- 查询某个产品类别的销售总额
SELECT product_category_id, category_name, study.sales_system_pkg.get_category_sales_total(product_category_id) AS sales_total
FROM study.product_categories;

-- 查询某个客户的订单列表
DECLARE
  orders_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_customer_orders(1, orders_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个产品的销售详情
DECLARE
  sales_details_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_product_sales_details(1, sales_details_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个部门的员工列表
DECLARE
  employees_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_department_employees(1, employees_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个产品的销售员工列表
DECLARE
  employees_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_product_sales_employees(1, employees_cursor);
  -- 可以在此处使用游标
END;


-- 查询统计前的执行计划
EXEC dbms_stats.delete_schema_stats(User);
SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE 
 FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
explain plan for SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE
  FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
  SELECT * FROM table(dbms_xplan.display);
  
  
  -- 使用直方图统计
EXEC dbms_stats.gather_schema_stats(User,method_opt=>'for all columns SIZE 250',cascade=>TRUE);
explain plan for 
SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE 
  FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
SELECT * FROM table(dbms_xplan.display);