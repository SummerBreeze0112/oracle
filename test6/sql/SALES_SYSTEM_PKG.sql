CREATE OR REPLACE EDITIONABLE PACKAGE "STUDY"."SALES_SYSTEM_PKG" AS
  -- 查询某个员工的部门名称
  FUNCTION get_employee_department(employeeID  NUMBER) RETURN VARCHAR2;

  -- 查询某个客户的订单总金额
  FUNCTION get_customer_order_total(customerID IN NUMBER) RETURN NUMBER;

  -- 查询某个客户的订单数量
  FUNCTION get_customer_order_count(customerID IN NUMBER) RETURN NUMBER;

  -- 查询某个员工的销售总额
  FUNCTION get_employee_sales_total(employeeID IN NUMBER) RETURN NUMBER;

  -- 查询某个产品的销售数量
  FUNCTION get_product_sales_count(productID IN NUMBER) RETURN NUMBER;

  -- 查询某个产品类别的销售总额
  FUNCTION get_category_sales_total(categoryID IN NUMBER) RETURN NUMBER;

  -- 查询某个客户的订单列表
  PROCEDURE get_customer_orders(customerID IN NUMBER, orders_cursor OUT SYS_REFCURSOR);

  -- 查询某个产品的销售详情
  PROCEDURE get_product_sales_details(productID IN NUMBER, sales_details_cursor OUT SYS_REFCURSOR);

  -- 查询某个部门的员工列表
  PROCEDURE get_department_employees(departmentID IN NUMBER, employees_cursor OUT SYS_REFCURSOR);

  -- 查询某个产品的销售员工列表
  PROCEDURE get_product_sales_employees(productID IN NUMBER, employees_cursor OUT SYS_REFCURSOR);

END sales_system_pkg;

/

CREATE OR REPLACE EDITIONABLE PACKAGE BODY "STUDY"."SALES_SYSTEM_PKG" AS

  -- 查询某个员工的部门名称
  FUNCTION get_employee_department(employeeID  NUMBER) RETURN VARCHAR2 AS
    department_name VARCHAR2(40);
  BEGIN
    SELECT d.DEPARTMENT_NAME INTO department_name
    FROM STUDY.DEPARTMENTS d,STUDY.EMPLOYEES e 
    WHERE e.EMPLOYEE_ID = employeeID and  d.DEPARTMENT_ID = e.DEPARTMENT_ID;
    RETURN department_name;
  END get_employee_department;

  -- 查询某个客户的订单总金额
  FUNCTION get_customer_order_total(customerID IN NUMBER) RETURN NUMBER IS
    order_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO order_total
    FROM STUDY.ORDERS
    WHERE CUSTOMER_ID = customerID;

    RETURN order_total;
  END get_customer_order_total;

  -- 查询某个客户的订单数量
  FUNCTION get_customer_order_count(customerID IN NUMBER) RETURN NUMBER IS
    order_count NUMBER;
  BEGIN
    SELECT COUNT(*) INTO order_count
    FROM STUDY.ORDERS
    WHERE CUSTOMER_ID = customerID;

    RETURN order_count;
  END get_customer_order_count;

  -- 查询某个员工的销售总额
  FUNCTION get_employee_sales_total(employeeID IN NUMBER) RETURN NUMBER IS
    sales_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO sales_total
    FROM STUDY.ORDERS
    WHERE EMPLOYEE_ID = employeeID;

    RETURN sales_total;
  END get_employee_sales_total;

  -- 查询某个产品的销售数量
  FUNCTION get_product_sales_count(productID IN NUMBER) RETURN NUMBER IS
    sales_count NUMBER;
  BEGIN
    SELECT SUM(QUANTITY) INTO sales_count
    FROM STUDY.ORDER_DETAILS
    WHERE PRODUCT_ID = productID;

    RETURN sales_count;
  END get_product_sales_count;

  -- 查询某个产品类别的销售总额
  FUNCTION get_category_sales_total(categoryID IN NUMBER) RETURN NUMBER IS
    sales_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO sales_total
    FROM STUDY.ORDERS o
    JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
    JOIN STUDY.PRODUCTS p ON od.PRODUCT_ID = p.PRODUCT_ID
    WHERE p.PRODUCT_CATEGORY_ID = categoryID;

    RETURN sales_total;
  END get_category_sales_total;

  -- 查询某个客户的订单列表
  PROCEDURE get_customer_orders(customerID IN NUMBER, orders_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN orders_cursor FOR
      SELECT o.ORDER_ID, o.ORDER_DATE, p.PRODUCT_NAME, od.QUANTITY, od.UNIT_PRICE
      FROM STUDY.ORDERS o
      JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
      JOIN STUDY.PRODUCTS p ON od.PRODUCT_ID = p.PRODUCT_ID
      WHERE o.CUSTOMER_ID = customerID;
  END get_customer_orders;

  -- 查询某个产品的销售详情
  PROCEDURE get_product_sales_details(productID IN NUMBER, sales_details_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN sales_details_cursor FOR
      SELECT o.ORDER_DATE, c.CUSTOMER_NAME, od.QUANTITY, od.UNIT_PRICE
      FROM STUDY.ORDER_DETAILS od
      JOIN STUDY.ORDERS o ON od.ORDER_ID = o.ORDER_ID
      JOIN STUDY.CUSTOMER c ON o.CUSTOMER_ID = c.CUSTOMER_ID
      WHERE od.PRODUCT_ID = productID;
  END get_product_sales_details;

  -- 查询某个部门的员工列表
  PROCEDURE get_department_employees(departmentID IN NUMBER, employees_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN employees_cursor FOR
      SELECT e.EMPLOYEE_ID, e.NAME, e.EMAIL, e.PHONE_NUMBER, e.HIRE_DATE, e.SALARY
      FROM STUDY.EMPLOYEES e
      WHERE e.DEPARTMENT_ID = departmentID;
  END get_department_employees;

  -- 查询某个产品的销售员工列表
  PROCEDURE get_product_sales_employees(productID IN NUMBER, employees_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN employees_cursor FOR
      SELECT e.EMPLOYEE_ID, e.NAME, e.EMAIL, e.PHONE_NUMBER, e.HIRE_DATE, e.SALARY
      FROM STUDY.EMPLOYEES e
      JOIN STUDY.ORDERS o ON e.EMPLOYEE_ID = o.EMPLOYEE_ID
      JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
      WHERE od.PRODUCT_ID = productID;
  END get_product_sales_employees;

END sales_system_pkg;

/