CREATE TABLE STUDY.CUSTOMER 
(
  CUSTOMER_ID NUMBER(10, 0) NOT NULL 
, CUSTOMER_NAME VARCHAR2(100 BYTE) NOT NULL 
, ADDRESS VARCHAR2(500 BYTE) 
, EMAIL VARCHAR2(100 BYTE) NOT NULL 
, PHONE VARCHAR2(20 BYTE) 
, CONSTRAINT SYS_C007569 PRIMARY KEY 
  (
    CUSTOMER_ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX STUDY.SYS_C007569 ON STUDY.CUSTOMER (CUSTOMER_ID ASC) 
      LOGGING 
      TABLESPACE USERS 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        NEXT 1048576 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;
