CREATE TABLE STUDY.EMPLOYEES 
(
  EMPLOYEE_ID NUMBER(6, 0) NOT NULL 
, NAME VARCHAR2(40 BYTE) NOT NULL 
, EMAIL VARCHAR2(40 BYTE) 
, PHONE_NUMBER VARCHAR2(40 BYTE) 
, HIRE_DATE DATE NOT NULL 
, SALARY NUMBER(8, 2) 
, MANAGER_ID NUMBER(6, 0) 
, DEPARTMENT_ID NUMBER(6, 0) 
, PHOTO BLOB 
, CONSTRAINT SYS_C007576 PRIMARY KEY 
  (
    EMPLOYEE_ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX STUDY.SYS_C007576 ON STUDY.EMPLOYEES (EMPLOYEE_ID ASC) 
      LOGGING 
      TABLESPACE USERS 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        NEXT 1048576 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL 
LOB (PHOTO) STORE AS SYS_LOB0000073504C00009$$ 
( 
  ENABLE STORAGE IN ROW 
  CHUNK 8192 
  NOCACHE 
  LOGGING  
);

CREATE INDEX EMPLOYEES_NAME_IDX ON STUDY.EMPLOYEES (NAME ASC) 
LOGGING 
TABLESPACE SYSTEM 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  FREELISTS 1 
  FREELIST GROUPS 1 
  BUFFER_POOL DEFAULT 
) 
NOPARALLEL;

ALTER TABLE STUDY.EMPLOYEES
ADD CONSTRAINT SYS_C007577 FOREIGN KEY
(
  MANAGER_ID 
)
REFERENCES STUDY.EMPLOYEES
(
  EMPLOYEE_ID 
)
ENABLE;

ALTER TABLE STUDY.EMPLOYEES
ADD CONSTRAINT SYS_C007578 FOREIGN KEY
(
  DEPARTMENT_ID 
)
REFERENCES STUDY.DEPARTMENTS
(
  DEPARTMENT_ID 
)
ENABLE;
