BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO STUDY.PRODUCT_CATEGORIES (PRODUCT_CATEGORY_ID, CATEGORY_NAME, CATEGORY_DESCRIPTION)
    VALUES (i, 'Category ' || i, 'Description for Category ' || i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO STUDY.PRODUCTS (PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, PRICE, STOCK, PRODUCT_CATEGORY_ID)
    VALUES (i, 'Product ' || i, 'Description for Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO STUDY.CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, EMAIL, PHONE)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'customer' || i || '@example.com', '1234567890');
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..10 LOOP
    INSERT INTO STUDY.DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME)
    VALUES (i, 'Department ' || i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO STUDY.EMPLOYEES (EMPLOYEE_ID, NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, SALARY, DEPARTMENT_ID)
    VALUES (i, 'Employee ' || i, 'employee' || i || '@example.com', '1234567890', SYSDATE, ROUND(DBMS_RANDOM.VALUE(1000, 5000), 2), ROUND(DBMS_RANDOM.VALUE(1, 10), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO STUDY.ORDERS (ORDER_ID, CUSTOMER_ID, EMPLOYEE_ID, ORDER_DATE, DISCOUNT, TRADE_RECEIVABLE)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 0), TO_DATE('2015-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(0, 2922), ROUND(DBMS_RANDOM.VALUE(0, 10), 2), ROUND(DBMS_RANDOM.VALUE(100, 10000), 2)); -- 2015 年到 2024 年之间随机日期
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO STUDY.ORDER_DETAILS (ORDER_DETAIL_ID, ORDER_ID, PRODUCT_ID, QUANTITY, UNIT_PRICE, PAY_STATUS)
    VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 0), ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(0, 1), 0));
  END LOOP;
  COMMIT;
END;