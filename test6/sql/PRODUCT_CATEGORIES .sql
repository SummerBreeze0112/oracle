CREATE TABLE STUDY.PRODUCT_CATEGORIES 
(
  PRODUCT_CATEGORY_ID NUMBER(10, 0) NOT NULL 
, CATEGORY_NAME VARCHAR2(100 BYTE) NOT NULL 
, CATEGORY_DESCRIPTION VARCHAR2(500 BYTE) 
, CONSTRAINT SYS_C007558 PRIMARY KEY 
  (
    PRODUCT_CATEGORY_ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX STUDY.SYS_C007558 ON STUDY.PRODUCT_CATEGORIES (PRODUCT_CATEGORY_ID ASC) 
      LOGGING 
      TABLESPACE USERS 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        INITIAL 65536 
        NEXT 1048576 
        MINEXTENTS 1 
        MAXEXTENTS UNLIMITED 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;
