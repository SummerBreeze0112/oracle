# 个人信息

班级：2020级软件工程2班

学号：202010414229

姓名：周钰全

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

# 1  项目概述

## 1.1   项目背景

在现代商业环境中，商品销售系统是企业进行销售、库存管理和客户关系管理的关键组成部分。为了提高销售效率和准确性，许多企业选择使用数据库系统来设计和管理他们的销售系统。本报告旨在设计一个基于Oracle数据库的商品销售系统，以满足企业的销售需求。

## 1.2   设计目的

设计目的是开发一个功能完善、高效可靠的商品销售系统，通过集成销售、库存管理和客户关系管理功能，实现订单处理、库存跟踪、销售分析和客户管理等关键业务流程。系统设计还考虑用户友好性、安全性和可扩展性，以满足企业日益增长的销售需求。

## 1.3   开发工具选择

系统开发语言：Java

数据库语言：Oracle

系统开发平台：IntelliJ IDEA

数据库管理软件：Oracle SQL Developer

据库管理软件：Oracle SQL Developer

# 2  表设计

## 2.1   表结构说明

**1)** **商品类别表（PRODUCT_CATEGORIES）**

| **字段名**           | **数据类型**       | **可以为空** | **注释**       |
| -------------------- | ------------------ | ------------ | -------------- |
| PRODUCT_CATEGORY_ID  | NUMBER(10,0)       | NO           | 类别ID号，主键 |
| CATEGORY_NAME        | VARCHAR2(100 BYTE) | NO           | 类别名称       |
| CATEGORY_DESCRIPTION | VARCHAR2(500 BYTE) | YES          | 类别描述       |

**2)** **商品表（PRODUCTS）**

| **字段名**          | **数据类型**       | **可以为空** | **注释**                         |
| ------------------- | ------------------ | ------------ | -------------------------------- |
| PRODUCT_ID          | NUMBER(10,0)       | NO           | 商品ID号，商品表的主键           |
| PRODUCT_NAME        | VARCHAR2(100 BYTE) | NO           | 商品名称                         |
| PRODUCT_DESCRIPTION | VARCHAR2(500 BYTE) | YES          | 商品描述                         |
| PRICE               | NUMBER(10,2)       | NO           | 商品价格，精确到小数点后两位     |
| STOCK               | NUMBER(10,0)       | NO           | 商品库存数量                     |
| PRODUCT_CATEGORY_ID | NUMBER(10,0)       | NO           | 类别ID号，关联到商品类别表的外键 |

**3)** **客户表（CUSTOMER）**

| **字段名**    | **数据类型**       | **可以为空** | **注释**       |
| ------------- | ------------------ | ------------ | -------------- |
| CUSTOMER_ID   | NUMBER(10,0)       | NO           | 客户ID号，主键 |
| CUSTOMER_NAME | VARCHAR2(100 BYTE) | NO           | 客户姓名       |
| ADDRESS       | VARCHAR2(500 BYTE) | YES          | 客户地址       |
| EMAIL         | VARCHAR2(100 BYTE) | NO           | 客户电子邮件   |
| PHONE         | VARCHAR2(20 BYTE)  | YES          | 客户电话号码   |

**4)** **部门表（DEPARTMENTS）**

| 字段名           | 数据类型          | 可以为空 | 注释         |
| ---------------- | ----------------- | -------- | ------------ |
| DEPARTMENT_ID    | NUMBER(6,0)       | NO       | 部门ID，主键 |
| DEPARTMENT_NAME* | VARCHAR2(40 BYTE) | NO       | 部门名称     |

**5) 员工表（EMPLOYEES）**

| **字段名**    | **数据类型**      | **可以为空** | **注释**                                                     |
| ------------- | ----------------- | ------------ | :----------------------------------------------------------- |
| EMPLOYEE_ID   | NUMBER(6,0)       | NO           | 员工ID，员工表的主键                                         |
| NAME          | VARCHAR2(40 BYTE) | NO           | 员工姓名，不能为空，创建不唯一B树索引                        |
| EMAIL         | VARCHAR2(40 BYTE) | YES          | 电子信箱                                                     |
| PHONE_NUMBER  | VARCHAR2(40 BYTE) | YES          | 电话                                                         |
| HIRE_DATE     | DATE              | NO           | 雇佣日期                                                     |
| SALARY        | NUMBER(8,2)       | YES          | 月薪，必须>0                                                 |
| MANAGER_ID    | NUMBER(6,0)       | YES          | 员工的上司，员工表EMPOLYEE_ID的外键，MANAGER_ID不能等于EMPLOYEE_ID |
| DEPARTMENT_ID | NUMBER(6,0)       | YES          | 员工所在部门，是部门表DEPARTMENTS的外键                      |
| PHOTO         | BLOB              | YES          | 员工照片                                                     |

**6)** **订单表（ORDERS）**

| 字段名           | 数据类型     | 可以为空 | 注释                                      |
| ---------------- | ------------ | -------- | ----------------------------------------- |
| ORDER_ID         | NUMBER(10,0) | NO       | 订单ID号，主键                            |
| CUSTOMER_ID      | NUMBER(10,0) | NO       | 客户ID号，关联到客户表的外键，创建B树索引 |
| EMPLOYEE_ID      | NUMBER(6,0)  | NO       | 订单经手人，员工表EMPLOYEES的外键         |
| ORDER_DATE       | DATE         | NO       | 订单日期，按年份采用分区存储方式          |
| DISCOUNT         | NUMBER(8,2)  | YES      | 订单整体优惠金额。默认值为0               |
| TRADE_RECEIVABLE | NUMBER(8,2)  | NO       | 订单总金额，精确到小数点后两位            |

**7)** **订单详情表（ORDER_DETAILS）**

| **字段名**      | **数据类型** | **可以为空** | **注释**                          |
| --------------- | ------------ | ------------ | --------------------------------- |
| ORDER_DETAIL_ID | NUMBER(10,0) | NO           | 订单详情ID号，主键                |
| ORDER_ID        | NUMBER(10,0) | NO           | 订单ID号，关联到订单表的外键      |
| PRODUCT_ID      | NUMBER(10,0) | NO           | 商品ID号，关联到商品表的外键      |
| QUANTITY        | NUMBER(10,0) | NO           | 销售数量，必须>0                  |
| UNIT_PRICE      | NUMBER(10,2) | NO           | 商品单价，精确到小数点后两位      |
| PAY_STATUS      | NUMBER(1,0)  | YES          | 支付状态。默认值为0，表示未支付。 |

**8) 临时订单ID表（ORDER_ID_TEMP）**

| **字段名** | **数据类型**  | **可以为空** | **注释** |
| ---------- | ------------- | ------------ | -------- |
| ORDER_ID   | NUMBER(10，0) | NO           | 主键     |



## **2.2   表之间的关系**

- **商品表（Product）与商品类别表（Category）之间存在一对多的关系，即一个商品类别可以对应多个商品，而一个商品只属于一个商品类别。**
- **订单表（Orders）与客户表（Customer）之间存在一对多的关系，即一个客户可以有多个订单，而一个订单只属于一个客户。**
- **商品表（Product）与订单详情表（OrderDetail）之间存在一对多的关系，即一个商品可以出现在多个订单详情中，而一个订单详情只对应一个商品。**

# **3  表空间设计**

## 3.1 分配用户和表空间

创建的一个表空间USERS02，给USERS02表空间分配了两个数据文件：pdbtest_users02_1.dbf和pdbtest_users02_2.dbf，这两个数据文件初始大小都是100M，所以表空间的初始大小是200M。

```sql
CREATE TABLESPACE Users02 
DATAFILE 
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf'
  SIZE 100M AUTOEXTEND ON NEXT 256M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_2.dbf' 
  SIZE 100M AUTOEXTEND ON NEXT 256M MAXSIZE UNLIMITED
    EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```

创建用户STUDY，并设置：

```sql
-- 创建用户 STUDY，使用密码 "123"
CREATE USER STUDY IDENTIFIED BY "123"
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";

-- 设置 STUDY 用户在 USERS、USERS02 表空间的配额为无限制
ALTER USER STUDY QUOTA UNLIMITED ON USERS;
ALTER USER STUDY QUOTA UNLIMITED ON USERS02;

-- 授予 STUDY 用户 CONNECT 角色和 RESOURCE 角色，并具有 ADMIN OPTION 权限
GRANT "CONNECT" TO STUDY WITH ADMIN OPTION;
GRANT "RESOURCE" TO STUDY WITH ADMIN OPTION;

-- 将 CONNECT 和 RESOURCE 角色设置为 STUDY 用户的默认角色
ALTER USER STUDY DEFAULT ROLE "CONNECT", "RESOURCE";

-- 授予 STUDY 用户创建视图的系统权限，并具有 ADMIN OPTION 权限
GRANT CREATE VIEW TO STUDY WITH ADMIN OPTION;
```

## 3.2 创建表、约束和索引

在用户和空间分配完成之后，可以创建表，约束和索引。

由于订单表和订单详单表的记录数量非常大，可能上千万条记录，所以把ORDERS和ORDER_DETAILS两张表设计为基于ORDER_DATE的分区表，来加快查询速度。表ORDER_DETAILS使用了基于外键ORDER_ID的引用分区，这个外键引用了另一个表 "ORDERS" 的 "order_id" 列。所以 "ORDER_DETAILS" 表的数据将根据 "ORDERS" 表的分区自动分区。

以下是每个表的创建的SQL语句，还为员工表的NAME字段（员工姓名），创建不唯一B树索引，为订单表的CUSTOMER_ID（客户ID号），创建B树索引。

```sql
CREATE TABLE STUDY.PRODUCT_CATEGORIES (
  PRODUCT_CATEGORY_ID  NUMBER(10,0)       NOT NULL,
  CATEGORY_NAME        VARCHAR2(100 BYTE) NOT NULL,
  CATEGORY_DESCRIPTION VARCHAR2(500 BYTE),
  PRIMARY KEY (PRODUCT_CATEGORY_ID)
);


CREATE TABLE STUDY.PRODUCTS (
  PRODUCT_ID          NUMBER(10,0)       NOT NULL,
  PRODUCT_NAME        VARCHAR2(100 BYTE) NOT NULL,
  PRODUCT_DESCRIPTION VARCHAR2(500 BYTE),
  PRICE               NUMBER(10,2)       NOT NULL,
  STOCK               NUMBER(10,0)       NOT NULL,
  PRODUCT_CATEGORY_ID NUMBER(10,0)       NOT NULL,
  PRIMARY KEY (PRODUCT_ID),
  FOREIGN KEY (PRODUCT_CATEGORY_ID) REFERENCES STUDY.PRODUCT_CATEGORIES (PRODUCT_CATEGORY_ID)
);



CREATE TABLE STUDY.CUSTOMER (
  CUSTOMER_ID   NUMBER(10,0)       NOT NULL,
  CUSTOMER_NAME VARCHAR2(100 BYTE) NOT NULL,
  ADDRESS       VARCHAR2(500 BYTE),
  EMAIL         VARCHAR2(100 BYTE) NOT NULL,
  PHONE         VARCHAR2(20 BYTE),
  PRIMARY KEY (CUSTOMER_ID)
);





CREATE TABLE STUDY.DEPARTMENTS (
  DEPARTMENT_ID    NUMBER(6,0)       NOT NULL,
  DEPARTMENT_NAME  VARCHAR2(40 BYTE) NOT NULL,
  PRIMARY KEY (DEPARTMENT_ID)
);




CREATE TABLE STUDY.EMPLOYEES (
  EMPLOYEE_ID   NUMBER(6,0)       NOT NULL,
  NAME          VARCHAR2(40 BYTE) NOT NULL,
  EMAIL         VARCHAR2(40 BYTE),
  PHONE_NUMBER  VARCHAR2(40 BYTE),
  HIRE_DATE     DATE              NOT NULL,
  SALARY        NUMBER(8,2),
  MANAGER_ID    NUMBER(6,0),
  DEPARTMENT_ID NUMBER(6,0),
  PHOTO         BLOB,
  PRIMARY KEY (EMPLOYEE_ID),
  FOREIGN KEY (MANAGER_ID) REFERENCES STUDY.EMPLOYEES (EMPLOYEE_ID),
  FOREIGN KEY (DEPARTMENT_ID) REFERENCES STUDY.DEPARTMENTS (DEPARTMENT_ID)
);

-- 创建B树索引
CREATE INDEX EMPLOYEES_NAME_IDX ON STUDY.EMPLOYEES (NAME);




CREATE TABLE STUDY.ORDERS (
  ORDER_ID         NUMBER(10,0) NOT NULL,
  CUSTOMER_ID      NUMBER(10,0) NOT NULL,
  EMPLOYEE_ID      NUMBER(6,0)  NOT NULL,
  ORDER_DATE       DATE         NOT NULL,
  DISCOUNT         NUMBER(8,2)  DEFAULT 0,
  TRADE_RECEIVABLE NUMBER(8,2)  NOT NULL,
  PRIMARY KEY (ORDER_ID),
  FOREIGN KEY (CUSTOMER_ID) REFERENCES STUDY.CUSTOMER (CUSTOMER_ID),
  FOREIGN KEY (EMPLOYEE_ID) REFERENCES STUDY.EMPLOYEES (EMPLOYEE_ID)
)
-- 将表存储在名为"USERS"的表空间
TABLESPACE USERS 
-- 每个数据块会预留10%的空间，以防止数据插入时出现数据块溢出
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 
-- 根据订单日期范围对订单表进行分区
PARTITION BY RANGE (ORDER_DATE)
(
  PARTITION ORDERS_BEFORE_2016 VALUES LESS THAN (TO_DATE('2016-01-01', 'YYYY-MM-DD')),
  PARTITION ORDERS_2016_TO_2021 VALUES LESS THAN (TO_DATE('2022-01-01', 'YYYY-MM-DD')),
  PARTITION ORDERS_2022 VALUES LESS THAN (TO_DATE('2023-01-01', 'YYYY-MM-DD')),
  PARTITION ORDERS_2023 VALUES LESS THAN (MAXVALUE)
);

CREATE INDEX ORDERS_CUSTOMER_IDX ON STUDY.ORDERS (CUSTOMER_ID ASC);



CREATE TABLE STUDY.ORDER_DETAILS (
  ORDER_DETAIL_ID NUMBER(10,0) NOT NULL,
  ORDER_ID        NUMBER(10,0) NOT NULL,
  PRODUCT_ID      NUMBER(10,0) NOT NULL,
  QUANTITY        NUMBER(10,0) NOT NULL,
  UNIT_PRICE      NUMBER(10,2) NOT NULL,
  PAY_STATUS      NUMBER(1,0) DEFAULT 0,
  PRIMARY KEY (ORDER_DETAIL_ID),
  CONSTRAINT order_details_fk1 FOREIGN KEY (ORDER_ID) REFERENCES STUDY.ORDERS (ORDER_ID),
  FOREIGN KEY (PRODUCT_ID) REFERENCES STUDY.PRODUCTS (PRODUCT_ID)
)
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
-- 基于引用的分区，以order_details_fk1为依据进行分区
PARTITION BY REFERENCE (order_details_fk1);



-- 创建全局临时表 "ORDER_ID_TEMP"
CREATE GLOBAL TEMPORARY TABLE "ORDER_ID_TEMP" (
  ORDER_ID NUMBER(10,0) NOT NULL ENABLE,
  PRIMARY KEY (ORDER_ID)
)ON COMMIT DELETE ROWS ; -- 在事务提交时删除临时表的所有行
COMMENT ON TABLE "ORDER_ID_TEMP"  IS '用于触发器存储临时ORDER_ID';
```

## 3.3 **创建触发器、序列和视图**

每个表的主键设置了自增序列：

```sql
-- 创建自增序列
CREATE SEQUENCE STUDY.PRODUCT_CATEGORIES_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

-- 创建触发器，将表的主键与自增序列绑定
CREATE OR REPLACE TRIGGER STUDY.PRODUCT_CATEGORIES_TRIGGER
  BEFORE INSERT ON STUDY.PRODUCT_CATEGORIES
  FOR EACH ROW
BEGIN
  :NEW.PRODUCT_CATEGORY_ID := PRODUCT_CATEGORIES_SEQ.NEXTVAL;
END;



CREATE SEQUENCE STUDY.PRODUCTS_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.PRODUCTS_TRIGGER
  BEFORE INSERT ON PRODUCTS
  FOR EACH ROW
BEGIN
  :NEW.PRODUCT_ID := PRODUCTS_SEQ.NEXTVAL;
END;


CREATE SEQUENCE STUDY.CUSTOMER_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.CUSTOMER_TRIGGER
  BEFORE INSERT ON CUSTOMER
  FOR EACH ROW
BEGIN
  :NEW.CUSTOMER_ID := CUSTOMER_SEQ.NEXTVAL;
END;


CREATE SEQUENCE STUDY.DEPARTMENTS_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.DEPARTMENTS_TRIGGER
  BEFORE INSERT ON DEPARTMENTS
  FOR EACH ROW
BEGIN
  :NEW.DEPARTMENT_ID := DEPARTMENTS_SEQ.NEXTVAL;
END;


CREATE SEQUENCE STUDY.EMPLOYEES_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.EMPLOYEES_TRIGGER
  BEFORE INSERT ON EMPLOYEES
  FOR EACH ROW
BEGIN
  :NEW.EMPLOYEE_ID := EMPLOYEES_SEQ.NEXTVAL;
END;


CREATE SEQUENCE STUDY.ORDERS_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.ORDERS_TRIGGER
  BEFORE INSERT ON ORDERS
  FOR EACH ROW
BEGIN
  :NEW.ORDER_ID := ORDERS_SEQ.NEXTVAL;
END;


CREATE SEQUENCE STUDY.ORDER_DETAIL_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE OR REPLACE TRIGGER STUDY.ORDER_DETAIL_TRIGGER
  BEFORE INSERT ON ORDER_DETAILS
  FOR EACH ROW
BEGIN
  :NEW.ORDER_DETAIL_ID := ORDER_DETAIL_SEQ.NEXTVAL;
END;
```

本销售系统中订单表ORDERS的TRADE_RECEIVABLE是应收货款，是自动计算字段，它的计算公式是：

Trade_Receivable=sum(ORDER_DETAILS.Product_Num*ORDER_DETAILS.Product_Price)- Discount。

从这个公式可以看出，当修改订单表的折扣Discount或者对订单详单表ORDER_DETAILS作任何增加，修改，删除的操作后，都可能需要重新计算Trade_Receivable。

以下是3个触发器，来完成这一需求：

```sql
-- 对Orders表进行Insert,Update之前进行的行级触发器，作用是统计每个触发订单的应收货款。
CREATE OR REPLACE EDITIONABLE TRIGGER STUDY.ORDERS_TRIG_ROW_LEVEL
BEFORE INSERT OR UPDATE OF DISCOUNT ON STUDY.ORDERS
FOR EACH ROW -- 行级触发器，在每个行级操作触发之前执行
declare
  m number(8,2); 
BEGIN
  if inserting then -- 如果是插入操作
      :new.TRADE_RECEIVABLE := - :new.discount; -- 将新插入行的TRADE_RECEIVABLE字段设置为负的DISCOUNT值
  else -- 如果是更新操作
      SELECT sum(QUANTITY*UNIT_PRICE) INTO m FROM 
         ORDER_DETAILS WHERE ORDER_ID = :old.ORDER_ID; -- 查询对应订单ID的订单详情中数量和单价的乘积之和，并将结果存储在变量m中
      if m is null then -- 如果计算结果为空
        m := 0;
      end if;
     :new.TRADE_RECEIVABLE := m - :new.discount; -- 将新更新行的TRADE_RECEIVABLE字段设置为计算结果m减去DISCOUNT值
  end if;
END;


-- 对Order_Details表进行Delete,Insert或者Update之后进行的行级触发器。将有改变的ORDER_ID写入临时表ORDER_ID_TEMP 中，以便ORDER_DETAILS_SNTNS_TRIG触发器统计应收货款。
CREATE OR REPLACE EDITIONABLE TRIGGER STUDY.ORDER_DETAILS_ROW_TRIG
AFTER DELETE OR INSERT OR UPDATE ON STUDY.ORDER_DETAILS 
FOR EACH ROW 
BEGIN

  -- 如果新插入的行的"ORDER_ID"字段不为空，则将其插入到"ORDER_ID_TEMP"表中
  IF :NEW.ORDER_ID IS NOT NULL THEN
    MERGE INTO STUDY.ORDER_ID_TEMP A
    USING (SELECT 1 FROM DUAL) B
    ON (A.ORDER_ID = :NEW.ORDER_ID)
    WHEN NOT MATCHED THEN
      INSERT (ORDER_ID) VALUES (:NEW.ORDER_ID);
  END IF;

  -- 如果被更新或删除的行的"ORDER_ID"字段不为空，则将其插入到"ORDER_ID_TEMP"表中
  IF :OLD.ORDER_ID IS NOT NULL THEN
    MERGE INTO STUDY.ORDER_ID_TEMP A
    USING (SELECT 1 FROM DUAL) B
    ON (A.ORDER_ID = :OLD.ORDER_ID)
    WHEN NOT MATCHED THEN
      INSERT (ORDER_ID) VALUES (:OLD.ORDER_ID);
  END IF;
END;


-- 对Order_Details表进行Delete,Insert或者Update之后进行的统计应收货款的统计工作。该触发器代码中的ORDER_ID_TEMP表的数据来自于ORDER_DETAILS_ROW_TRIG触发器
CREATE OR REPLACE EDITIONABLE TRIGGER STUDY.ORDER_DETAILS_SNTNS_TRIG
AFTER DELETE OR INSERT OR UPDATE ON STUDY.ORDER_DETAILS 
DECLARE
  m NUMBER(8, 2); -- 声明变量 m，用于存储订单总金额
BEGIN
  FOR R IN (SELECT ORDER_ID FROM STUDY.ORDER_ID_TEMP) -- 循环遍历 ORDER_ID_TEMP 表中的订单ID
  LOOP
    -- 计算订单总金额并存储到变量 m 中
    SELECT SUM(QUANTITY*UNIT_PRICE) INTO m FROM STUDY.ORDER_DETAILS 
    WHERE ORDER_ID = R.ORDER_ID;
    
    -- 如果订单总金额为空，则将变量 m 设置为 0
    IF m IS NULL THEN
      m := 0;
    END IF;
    
    -- 更新 ORDERS 表中的 TRADE_RECEIVABLE 字段，减去折扣金额
    UPDATE STUDY.ORDERS SET TRADE_RECEIVABLE = m - discount 
    WHERE ORDER_ID = R.ORDER_ID;
  END LOOP;
  
  DELETE FROM STUDY.ORDER_ID_TEMP; -- 删除临时表来释放空间
END;
```

# 4  PL/SQL设计

以下是对本商品销售系统一些常用的查询函数，并将其封装在sales_system_pkg包中：

包头：

```sql
create or replace  PACKAGE sales_system_pkg AS
  -- 查询某个员工的部门名称
  FUNCTION get_employee_department(employeeID  NUMBER) RETURN VARCHAR2;

  -- 查询某个产品类别的名称
  FUNCTION get_category_name(categoryID IN NUMBER) RETURN VARCHAR2;

  -- 查询某个产品的库存数量
  FUNCTION get_product_stock(productID IN NUMBER) RETURN NUMBER;

  -- 查询某个客户的订单总金额
  FUNCTION get_customer_order_total(customerID IN NUMBER) RETURN NUMBER;

  -- 查询某个客户的订单数量
  FUNCTION get_customer_order_count(customerID IN NUMBER) RETURN NUMBER;

  -- 查询某个员工的销售总额
  FUNCTION get_employee_sales_total(employeeID IN NUMBER) RETURN NUMBER;

  -- 查询某个产品的销售数量
  FUNCTION get_product_sales_count(productID IN NUMBER) RETURN NUMBER;

  -- 查询某个产品类别的销售总额
  FUNCTION get_category_sales_total(categoryID IN NUMBER) RETURN NUMBER;

  -- 查询某个客户的订单列表
  PROCEDURE get_customer_orders(customerID IN NUMBER, orders_cursor OUT SYS_REFCURSOR);

  -- 查询某个产品的销售详情
  PROCEDURE get_product_sales_details(productID IN NUMBER, sales_details_cursor OUT SYS_REFCURSOR);

  -- 查询某个部门的员工列表
  PROCEDURE get_department_employees(departmentID IN NUMBER, employees_cursor OUT SYS_REFCURSOR);

  -- 查询某个产品的销售员工列表
  PROCEDURE get_product_sales_employees(productID IN NUMBER, employees_cursor OUT SYS_REFCURSOR);

END sales_system_pkg;
```

包体：

```sql
create or replace PACKAGE BODY sales_system_pkg AS

  -- 查询某个员工的部门名称
  FUNCTION get_employee_department(employeeID  NUMBER) RETURN VARCHAR2 AS
    department_name VARCHAR2(40);
  BEGIN
    SELECT d.DEPARTMENT_NAME INTO department_name
    FROM STUDY.DEPARTMENTS d,STUDY.EMPLOYEES e 
    WHERE e.EMPLOYEE_ID = employeeID and  d.DEPARTMENT_ID = e.DEPARTMENT_ID;
    RETURN department_name;
  END get_employee_department;

  -- 查询某个客户的订单总金额
  FUNCTION get_customer_order_total(customerID IN NUMBER) RETURN NUMBER IS
    order_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO order_total
    FROM STUDY.ORDERS
    WHERE CUSTOMER_ID = customerID;

    RETURN order_total;
  END get_customer_order_total;

  -- 查询某个客户的订单数量
  FUNCTION get_customer_order_count(customerID IN NUMBER) RETURN NUMBER IS
    order_count NUMBER;
  BEGIN
    SELECT COUNT(*) INTO order_count
    FROM STUDY.ORDERS
    WHERE CUSTOMER_ID = customerID;

    RETURN order_count;
  END get_customer_order_count;

  -- 查询某个员工的销售总额
  FUNCTION get_employee_sales_total(employeeID IN NUMBER) RETURN NUMBER IS
    sales_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO sales_total
    FROM STUDY.ORDERS
    WHERE EMPLOYEE_ID = employeeID;

    RETURN sales_total;
  END get_employee_sales_total;

  -- 查询某个产品的销售数量
  FUNCTION get_product_sales_count(productID IN NUMBER) RETURN NUMBER IS
    sales_count NUMBER;
  BEGIN
    SELECT SUM(QUANTITY) INTO sales_count
    FROM STUDY.ORDER_DETAILS
    WHERE PRODUCT_ID = productID;

    RETURN sales_count;
  END get_product_sales_count;

  -- 查询某个产品类别的销售总额
  FUNCTION get_category_sales_total(categoryID IN NUMBER) RETURN NUMBER IS
    sales_total NUMBER;
  BEGIN
    SELECT SUM(TRADE_RECEIVABLE) INTO sales_total
    FROM STUDY.ORDERS o
    JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
    JOIN STUDY.PRODUCTS p ON od.PRODUCT_ID = p.PRODUCT_ID
    WHERE p.PRODUCT_CATEGORY_ID = categoryID;

    RETURN sales_total;
  END get_category_sales_total;

  -- 查询某个客户的订单列表
  PROCEDURE get_customer_orders(customerID IN NUMBER, orders_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN orders_cursor FOR
      SELECT o.ORDER_ID, o.ORDER_DATE, p.PRODUCT_NAME, od.QUANTITY, od.UNIT_PRICE
      FROM STUDY.ORDERS o
      JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
      JOIN STUDY.PRODUCTS p ON od.PRODUCT_ID = p.PRODUCT_ID
      WHERE o.CUSTOMER_ID = customerID;
  END get_customer_orders;

  -- 查询某个产品的销售详情
  PROCEDURE get_product_sales_details(productID IN NUMBER, sales_details_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN sales_details_cursor FOR
      SELECT o.ORDER_DATE, c.CUSTOMER_NAME, od.QUANTITY, od.UNIT_PRICE
      FROM STUDY.ORDER_DETAILS od
      JOIN STUDY.ORDERS o ON od.ORDER_ID = o.ORDER_ID
      JOIN STUDY.CUSTOMER c ON o.CUSTOMER_ID = c.CUSTOMER_ID
      WHERE od.PRODUCT_ID = productID;
  END get_product_sales_details;

  -- 查询某个部门的员工列表
  PROCEDURE get_department_employees(departmentID IN NUMBER, employees_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN employees_cursor FOR
      SELECT e.EMPLOYEE_ID, e.NAME, e.EMAIL, e.PHONE_NUMBER, e.HIRE_DATE, e.SALARY
      FROM STUDY.EMPLOYEES e
      WHERE e.DEPARTMENT_ID = departmentID;
  END get_department_employees;

  -- 查询某个产品的销售员工列表
  PROCEDURE get_product_sales_employees(productID IN NUMBER, employees_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN employees_cursor FOR
      SELECT e.EMPLOYEE_ID, e.NAME, e.EMAIL, e.PHONE_NUMBER, e.HIRE_DATE, e.SALARY
      FROM STUDY.EMPLOYEES e
      JOIN STUDY.ORDERS o ON e.EMPLOYEE_ID = o.EMPLOYEE_ID
      JOIN STUDY.ORDER_DETAILS od ON o.ORDER_ID = od.ORDER_ID
      WHERE od.PRODUCT_ID = productID;
  END get_product_sales_employees;

END sales_system_pkg;
```



# 5  数据库测试

## 5.1 生成随机数据SQL

使用以下代码生成随机数据。商品类别表（PRODUCT_CATEGORIES）100行数据，商品表（PRODUCTS）1000行数据，客户表（CUSTOMER）1000行数据，部门表（DEPARTMENTS）10行数据，员工表（EMPLOYEES）100行数据，订单表（ORDERS）表插入10万行数据，订单详情表（ORDER_DETAIL）表插入10万行数据。

```sql
BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO STUDY.PRODUCT_CATEGORIES (PRODUCT_CATEGORY_ID, CATEGORY_NAME, CATEGORY_DESCRIPTION)
    VALUES (i, 'Category ' || i, 'Description for Category ' || i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO STUDY.PRODUCTS (PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, PRICE, STOCK, PRODUCT_CATEGORY_ID)
    VALUES (i, 'Product ' || i, 'Description for Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO STUDY.CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, ADDRESS, EMAIL, PHONE)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'customer' || i || '@example.com', '1234567890');
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..10 LOOP
    INSERT INTO STUDY.DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME)
    VALUES (i, 'Department ' || i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO STUDY.EMPLOYEES (EMPLOYEE_ID, NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, SALARY, DEPARTMENT_ID)
    VALUES (i, 'Employee ' || i, 'employee' || i || '@example.com', '1234567890', SYSDATE, ROUND(DBMS_RANDOM.VALUE(1000, 5000), 2), ROUND(DBMS_RANDOM.VALUE(1, 10), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO STUDY.ORDERS (ORDER_ID, CUSTOMER_ID, EMPLOYEE_ID, ORDER_DATE, DISCOUNT, TRADE_RECEIVABLE)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 0), TO_DATE('2015-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(0, 2922), ROUND(DBMS_RANDOM.VALUE(0, 10), 2), ROUND(DBMS_RANDOM.VALUE(100, 10000), 2)); -- 2015 年到 2024 年之间随机日期
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO STUDY.ORDER_DETAILS (ORDER_DETAIL_ID, ORDER_ID, PRODUCT_ID, QUANTITY, UNIT_PRICE, PAY_STATUS)
    VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 0), ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(0, 1), 0));
  END LOOP;
  COMMIT;
END;
```

## 5.2 测试程序包中的函数

测试代码：

```sql
-- 查询某个员工的部门名称
SELECT employee_id, name, study.sales_system_pkg.get_employee_department(employee_id) AS department_name
FROM study.employees;

-- 查询某个客户的订单总金额
SELECT customer_id, customer_name, study.sales_system_pkg.get_customer_order_total(customer_id) AS order_total
FROM study.customer;

-- 查询某个客户的订单数量
SELECT customer_id, customer_name, study.sales_system_pkg.get_customer_order_count(customer_id) AS order_count
FROM study.customer;

-- 查询某个员工的销售总额
SELECT employee_id, name, study.sales_system_pkg.get_employee_sales_total(employee_id) AS sales_total
FROM study.employees;

-- 查询某个产品的销售数量
SELECT product_id, product_name, study.sales_system_pkg.get_product_sales_count(product_id) AS sales_count
FROM study.products;

-- 查询某个产品类别的销售总额
SELECT product_category_id, category_name, study.sales_system_pkg.get_category_sales_total(product_category_id) AS sales_total
FROM study.product_categories;

-- 查询某个客户的订单列表
DECLARE
  orders_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_customer_orders(1, orders_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个产品的销售详情
DECLARE
  sales_details_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_product_sales_details(1, sales_details_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个部门的员工列表
DECLARE
  employees_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_department_employees(1, employees_cursor);
  -- 可以在此处使用游标
END;

-- 查询某个产品的销售员工列表
DECLARE
  employees_cursor SYS_REFCURSOR;
BEGIN
  study.sales_system_pkg.get_product_sales_employees(1, employees_cursor);
  -- 可以在此处使用游标
END;
```

测试效果截图：
![](pic01.png)

![pic02](pic02.png)

![pic03](pic03.png)

![pic04](pic04.png)

![pic05](pic05.png)

![pic06](pic06.png)

![pic07](pic07.png)

![pic08](pic08.png)

![pic09](pic09.png)

## 5.3 其他测试

直方图统计前后的对比查询：
代码：

```sql
-- 查询统计前的执行计划
EXEC dbms_stats.delete_schema_stats(User);
SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE 
 FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
explain plan for SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE
  FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
  SELECT * FROM table(dbms_xplan.display);
```

```sql
-- 使用直方图统计
EXEC dbms_stats.gather_schema_stats(User,method_opt=>'for all columns SIZE 250',cascade=>TRUE);
explain plan for 
SELECT ORDER_ID,ORDER_DATE,TRADE_RECEIVABLE 
  FROM STUDY.ORDERS WHERE TRADE_RECEIVABLE BETWEEN 500 AND 550;
SELECT * FROM table(dbms_xplan.display);
```

截图：
![](pic10.png)

![pic11](pic11.png)

分析：

没有使用直方图的执行计划：

- 操作（Operation）：全表扫描（TABLE ACCESS FULL）
- 表名（Name）：ORDERS
- 过滤条件（Predicate Information）：TRADE_RECEIVABLE >= 500 AND TRADE_RECEIVABLE <= 6000
- 执行计划（Plan）：
  - 全表扫描（TABLE ACCESS FULL）的代价（Cost）为 820
  - 预计返回的行数（Rows）为 16589
  - Pstart 和 Pstop 字段显示了分区范围，从 1 到 4

使用直方图的执行计划：

- 操作（Operation）：全表扫描（TABLE ACCESS FULL）
- 表名（Name）：ORDERS
- 过滤条件（Predicate Information）：TRADE_RECEIVABLE >= 500 AND TRADE_RECEIVABLE <= 525
- 执行计划（Plan）：
  - 全表扫描（TABLE ACCESS FULL）的代价（Cost）为 820
  - 预计返回的行数（Rows）为 1810
  - Pstart 和 Pstop 字段显示了分区范围，从 1 到 4

通过比较两个执行计划，可以看到使用直方图的执行计划在过滤条件上进行了优化。直方图提供了对列数据分布的统计信息，使得优化器可以更准确地估计查询结果集的大小，从而选择更有效的执行计划。在这种情况下，使用直方图的执行计划估计返回的行数更接近实际行数，可能会提高查询性能。

查询user_tables查看每个用户表的统计情况：

![](pic12.png)

分析：

查询结果中DEPARTMENTS、EMPLOYEES和PRODUCTS的表空间都是USERS，ORDERS和ORDER_DETAILS占用了两个表空间：USERS和USERS02，而ORDER_ID_TEMP占用的是临时表空间。
