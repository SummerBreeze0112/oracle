# 个人信息

班级：2020级软件工程2班

学号：202010414229

姓名：周钰全

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

1. 以system登录到pdborcl，创建角色con_res_role_zyq和用户sale_zyq，并授权和分配空间：

   ```sql
   $ sqlplus system/123@pdborcl
   CREATE ROLE con_res_role_zyq;
   GRANT connect,resource,CREATE VIEW TO con_res_role_zyq;
   CREATE USER sale_zyq IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
   ALTER USER sale_zyq default TABLESPACE "USERS";
   ALTER USER sale_zyq QUOTA 50M ON users;
   GRANT con_res_role_zyq TO sale_zyq;
   ```

   ![](pic01.png)

2. 新用户sale_zyq连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

   ```sql
   $ sqlplus sale_zyq/123@pdborcl
   SQL> show user;
   USER is "sale_zyq"
   SQL>
   SELECT * FROM session_privs;
   SELECT * FROM session_roles;
   SQL>
   CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
   INSERT INTO customers(id,name)VALUES(1,'zhang');
   INSERT INTO customers(id,name)VALUES (2,'wang');
   CREATE VIEW customers_view AS SELECT name FROM customers;
   GRANT SELECT ON customers_view TO hr;
   SELECT * FROM customers_view;
   ```

   ![](pic02.png)

   

3. 测试用户hr，sale_zyq之间的表的共享；用户hr连接到pdborcl，查询sale_zyq授予它的视图customers_view

   ```sql
   $ sqlplus hr/123@pdborcl
   SQL> SELECT * FROM sale_zyq.customers_view;
   ```

   此时，因为还没有授权hr访问显示view视图的权限，所以显示错误：

   ![](pic03.png)

   再去授予权限：

   ```sql
   $ sqlplus sale_zyq/123@pdborcl
   SQL> GRANT SELECT ON customers_view TO hr;
   ```

   ![](pic04.png)

   此时再使用用户hr，去访问sale_zyq的视图customers_view：

   ```sql
   $ sqlplus hr/123@pdborcl
   SQL> SELECT * FROM sale_zyq.customers_view;
   ```

   ![](pic05.png)

   另外，因为sale_zyq用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers，也没有办法进行修改。

   ```sql
   $ sqlplus hr/123@pdborcl
   SQL> SELECT * FROM sale_zyq.customers;
   ```

   ![](pic06.png)

   

4. 将sale_zyq用户的customers表的查询、修改、删除权限赋给hr用户：

   有两种方式：

   - 命令行：

     ```sql
     $ sqlplus sale_zyq/123@pdborcl
     SQL> GRANT SELECT ON customers TO hr;
     SQL> GRANT UPDATE ON customers TO hr;
     SQL> GRANT DELETE ON customers TO hr;
     SQL> GRANT INSERT ON customers TO hr;
     ```

     ![](pic11.png)

   - 使用SQL-DEVELOPER来进行授权：

     打开SQL-DEVELOPER，选择pdborcl_system，再点击其他用户，选择SALE_ZYQ，展开，再展开表，选择CUSTOMERS，再右键，再点击权限，再点击授权，之后进行授权操作：

     ![](pic09.png)

   读写共享测试：

   ```sql
   $ sqlplus hr/123@pdborcl
   SQL> INSERT INTO sale_zyq.customers(id,name)VALUES (4,'tian');
   SQL> UPDATE sale_zyq.customers SET name = 'li' WHERE id = 4;
   SQL> DELETE FROM sale_zyq.customers WHERE id = 1;
   SQL> SELECT * FROM sale_zyq.customers;
   ```

   ![](pic10.png)

5. 概要文件设置，设置用户最多登录时最多只能错误输入3次。

   ```sql
   $ sqlplus system/123@pdborcl
   SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
   ```

   - 设置后，使用"sqlplus hr/错误密码@pdborcl"进行错误登录 。3次错误密码后，用户被锁定。
   - 锁定后，通过system用户登录，再使用"alter user sale_zyq unlock"命令解锁。

   ```sql
   $ sqlplus system/123@pdborcl
   SQL> alter user sale_zyq  account unlock;
   ```

   ![](pic07.png)

6. 查看数据库的使用情况：

   查看表空间的数据库文件，以及每个文件的磁盘占用情况：

   ```sql
   $ sqlplus system/123@pdborcl
   
   SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';
   
   SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
    free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
    Round(( total - free )/ total,4)* 100 "使用率%"
    from (SELECT tablespace_name,Sum(bytes)free
           FROM   dba_free_space group  BY tablespace_name)a,
          (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
           group  BY tablespace_name)b
    where  a.tablespace_name = b.tablespace_name;
   ```

   ![](pic08.png)

7. 数据库和表空间占用分析：

   数据库pdborcl中包含了新的角色con_res_role_zyq和用户sale_zyq。新用户sale_zyq使用默认表空间users存储表的数据。随着用户往表中插入数据，表空间的磁盘使用量会增加。

8. 实验结束删除用户和角色：

   ```sql
   $ sqlplus system/123@pdborcl
   SQL>
   drop role con_res_role_zyq;
   drop user sale_zyq cascade;
   ```

![](pic12.png)
