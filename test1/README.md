# 个人信息

班级：2020级软件工程2班

学号：202010414229

姓名：周钰全

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

  

![](pict1.png)

- 查询工资高于10000且也高于所在部门平均工资的员工。

  查询1：

  ```sql
  set autotrace on
  
  select 
  concat(concat(e1.last_name,' '),e1.first_name) as "员工名字",d.department_name as "部门名称",e1.salary as "工资"
  from hr.employees e1,hr.departments d,
  (select department_id,AVG(salary) avg_salary
  from hr.employees
  group by department_id ) e2
  where d.department_id = e1.department_id and e1.department_id = e2.department_id and e1.salary>e2.avg_salary
  group by concat(concat(e1.last_name,' '),e1.first_name) ,d.department_name ,e1.salary
  having e1.salary>10000;
  ```

  输出结果：

  ![](pict4.png)

  ![](pict5.png)

  查询2：

  ```sql
  set autotrace on
  
  select 
  concat(concat(e1.last_name,' '),e1.first_name) as "员工名字",d.department_name as "部门名称",e1.salary as "工资"
  from hr.employees e1,hr.departments d,
  (select department_id,AVG(salary) avg_salary
  from hr.employees
  group by department_id ) e2
  where d.department_id = e1.department_id and e1.department_id = e2.department_id and e1.salary>e2.avg_salary and e1.salary>10000;
  ```

  输出结果：

  ![pict2](pict2.png)

  ![](pict3.png)

  

- 实验分析：

  从两次查询的统计信息进行对比，可以明显看出，查询2的性能呢要原优于查询1的。查询一多进行了624次递归调用，1021次一致性读，76次内存排序。究其原因也很明显，查询1使用having来对salary的大小进行限定，由于oracle语法限定，不得不使用group by先进行分组，这之间进行了许多没有必要的操作。

- SQL语句优化

  使用Oracle SQL Developer 进行SQL优化指导时，发现需要ADVISOR权限：

![](pict6.png)

​		于是使用命令行给予权限：

​		![](pict7.png)

​		执行SQL优化指导：

![](pict8.png)

该工具没有给出建议，说明语句接近最优。
